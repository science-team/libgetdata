GetData-0.10.0 introduces Dirfile Standards Version 10.  See below for details.
It also adds new functionality to the library and fixes bugs discovered since
the release of GetData-0.9.4.

The long-deprecated GetData-0.6 API has also been removed from this release.

---------------------------------------------------------------------------

Four packages are available:
* getdata-0.10.0.tar.bz2/.gz: the full source code to the library, with
    bindings.  This package uses the GNU autotools build system, and is
    designed for POSIX systems (UNIX, Linux, BSD, MacOS X, Cygwin, MSys,
    &c.)
* getdata_win-0.10.0.zip: a reduced source code package, with the CMake
    build system designed to be built on Microsoft Windows, either using
    the free MinGW compiler, or else Microsoft's Visual C++ compiler.
    (The full source package above can also be built using MinGW, if the
    MSys shell is used to run the build system.)  Currently, the only
    bindings provided by this package are the C++ bindings, and the
    package lacks support for compressed dirfiles, the Legacy API, and a
    few other features.  This build is used in native Microsoft Windows
    builds of kst2.
* idl_getdata-0.10.0.tar.bz2/.gz: the Interactive Data Language (IDL)
    bindings, packaged separately with an autotools build system, designed
    to be built against an already installed version of GetData.  Due to
    licensing restrictions, pre-built packages rarely come with these
    bindings, and this package allows end-users to add support for IDL
    without having to recompile the whole GetData package.
* matlab_getdata-0.10.0.tar.bz2/.gz: the MATLAB bindings, packaged
    separately with an autotools build system, designed to be built against
    an already installed version of GetData.  Due to licensing
    restrictions, pre-built packages rarely come with these bindings, and
    this package allows end- users to add support for MATLAB without having
    to recompile the whole GetData package.

---------------------------------------------------------------------------
